# SettingsData

- [SettingsData](#SettingsData)
    - [简介](#简介)
    - [约束](#约束)

## 简介
SettingsData是OpenHarmony预置的系统应用，为用户提供数据库的访问存取服务，例如存储/读取系统时间格式、屏幕亮度等系统属性。

## 约束
- 开发环境
    - **IDE**：DevEco Studio（版本号3.1.0.100）
    - **SDK**：Full-SDK（版本号3.2.10.10）

- 语言版本
    - [eTS](https://gitee.com/openharmony/docs/blob/master/zh-cn/application-dev/quick-start/start-with-ets.md)

- 建议
    -  推荐使用本工程下的签名配置，路径：signature目录下的所有签名文件。


