import AbilityStage from "@ohos.app.ability.AbilityStage"

export default class MyAbilityStage extends AbilityStage {
    onCreate() {
        console.info("[Demo] MyAbilityStage onCreate")
    }
}